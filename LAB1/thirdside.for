C		FIND THIRD SIDE OF THE RIGHT ANGLED TRIANGLE
		PROGRAM HYOUTENIOUS
		REAL P,B,H
		WRITE(*,*) 'ENTER P AND B:'
		READ *, P,B
C		CALLING SUBROUTINE CALCH
		CALL CALCH(P,B,H)
		WRITE(*,*) 'H: ',H
		END

C		DEFINING THE SUBROUTINE
		SUBROUTINE CALCH(X,Y,OUTPUT)
		REAL X,Y,OUTPUT
C		FOR RETURNING VALUE, WE SIMPLY PUT VALUE IN A RETURNING VARIABLE E.G. OUTPUT
		OUTPUT = SQRT(X**2 + Y**2)
		RETURN
		END